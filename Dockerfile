FROM node:16.17

#Create app directory, this is in our container/ in our image
WORKDIR /src/app

#Copy package json
COPY package*.json ./

#Install all dependencies
RUN  npm install

#Bundle app source
COPY . .

#script to start app
CMD ["npm", "run", "start"]

