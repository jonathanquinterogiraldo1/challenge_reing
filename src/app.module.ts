import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from '../ormconfig';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './exceptions/httpexception.filter';
import { NodeModule } from './node/node.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    NodeModule,
    MongooseModule.forRoot(
      'mongodb://nestuser:nestuser@mongo:27017/?authMechanism=DEFAULT',
    ),
    TypeOrmModule.forRoot(config),
    UsersModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule {}
