import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../services/users.service';
import { UsersController } from '../controllers/users.controller';
require('dotenv').config();

describe('UsersController', () => {
  let controller: UsersController;

  const TOKEN =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

  const user = {
    id: 1,
    name: 'Jonathan Quintero Giraldo',
    email: 'jonathan.quintero@gmail.com',
    password: '$2b$10$zjWS2xUjeNsZuLHe3MH9kuKgWcIg.C2frR8YvXqOCf6tw3lOhylN6',
    createdAt: '2022-09-25T20:33:57.490Z',
    updatedAt: '2022-09-25T20:33:57.490Z',
  };

  const mockUserService = {
    loginUser: jest.fn().mockResolvedValue({
      token: TOKEN,
    }),
    getUsers: jest.fn().mockResolvedValue([user, user]),
    getUser: jest.fn().mockResolvedValue(user),
    getOne: jest.fn().mockResolvedValue({ user }),
    createUser: jest.fn().mockResolvedValue(user),
    updateUser: jest.fn().mockResolvedValue(user),
    deleteUser: jest.fn(() => 'User deleted succesfully!'),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
      imports: [],
    })
      .overrideProvider(UsersService)
      .useValue(mockUserService)
      .compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return token', async () => {
    const userResquest = {
      email: 'test@test.com',
      password: '123456',
    };
    const response = await controller.loginUser(userResquest);
    expect(response.token).toBe(TOKEN);
  });

  it('should return list of users', async () => {
    const response = await controller.getAll();
    expect(response.length).toBe(2);
  });

  it('should return one user', async () => {
    const response = await controller.getOne(1);
    expect(response.id).toBe(1);
  });

  it('should return user created from postgres', async () => {
    const userResquest = {
      id: 1,
      name: 'nombre apellido1 apellido2',
      email: 'jonathan.quintero@gmail.com',
      password: '123456',
    };
    const response = await controller.createUser(userResquest);
    expect(response.id).toBe(1);
  });

  it('should return user updated', async () => {
    const userResquest = { email: 'jonathan.quintero@gmail.com' };
    const { id } = await controller.updateUser(userResquest, 1);
    expect(id).toBe(1);
  });

  it('should return messagee from user deleted', async () => {
    const userDeleted = await controller.deleteUser(1);
    expect(userDeleted).toBe('User deleted succesfully!');
  });
});
