import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from '../models/users.entity';
import {
  CreateUserDto,
  LoginUserRequestDto,
  TokenDto,
  UpdateUserDto,
} from '../dto/user.dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { TransactionResposeDto } from 'src/node/dto/node.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private userModel: Repository<Users>,
    private readonly jwtService: JwtService,
  ) {}

  private readonly logger = new Logger(UsersService.name);

  async loginUser(userData: LoginUserRequestDto): Promise<TokenDto> {
    try {
      const { email, password } = userData;
      const userDB: Users = await this.userModel.findOne({
        where: { email: email },
      });
      if (userDB) {
        const { password: hash, name } = userDB;
        const isMatch = await bcrypt.compare(password, hash);
        if (isMatch) {
          const token = await this.jwtService.sign({ name, email, password });
          this.logger.debug(`${email} logged successfully!`);
          return { token };
        } else
          throw new HttpException('Invalid Password', HttpStatus.BAD_REQUEST);
      } else throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async getUsers(): Promise<Users[]> {
    try {
      return await this.userModel.find();
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async getUser(id: number): Promise<Users> {
    try {
      return await this.userModel.findOne({ where: { id: id } });
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async createUser(createUserDTO: CreateUserDto): Promise<Users> {
    try {
      const { password, ...restData } = createUserDTO;
      const passwordEncrypted = await bcrypt.hash(password, 10);
      const newUser: Users = { password: passwordEncrypted, ...restData };
      const userSaved = await this.userModel.save(newUser);
      if (userSaved) {
        this.logger.debug(`User inserted on dababase successfully!`);
        return userSaved;
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async updateUser(id: number, updateUserDTO: UpdateUserDto): Promise<Users> {
    try {
      const updatedUser = await this.userModel.findOne({ where: { id: id } });
      this.userModel.merge(updatedUser, updateUserDTO);
      this.logger.debug(`User updated successfully!`);
      return this.userModel.save(updatedUser);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async deleteUser(id: number): Promise<TransactionResposeDto> {
    try {
      const userDeleted = await this.userModel.delete(id);
      if (userDeleted) {
        this.logger.debug(`User deleted successfully!`);
        return {
          message: 'User deleted successfully!',
        };
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
