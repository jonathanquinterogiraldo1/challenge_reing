import {
  Body,
  Controller,
  Delete,
  Get,
  UseGuards,
  Post,
  Put,
  Query,
  HttpCode,
  UseFilters,
} from '@nestjs/common';
import { HttpExceptionFilter } from '../../exceptions/httpexception.filter';
import { Users } from '../models/users.entity';
import { UsersService } from '../services/users.service';
import { JwtAuthGuard } from '../../auth/guards/jwt.guard';
import { CreateUserDto, LoginUserRequestDto, TokenDto } from '../dto/user.dto';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { TransactionResposeDto } from 'src/node/dto/node.dto';

@UseFilters(HttpExceptionFilter)
@Controller('api/users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @ApiCreatedResponse({ description: 'Jason Web Token (JWT) Sent' })
  @HttpCode(200)
  @Post('login')
  async loginUser(@Body() userData: LoginUserRequestDto): Promise<TokenDto> {
    return await this.userService.loginUser(userData);
  }

  @ApiCreatedResponse({ description: 'User list retorned successfully' })
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  @Get('all')
  async getAll(): Promise<Users[]> {
    return await this.userService.getUsers();
  }

  @ApiCreatedResponse({ description: 'User found successfully' })
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  @Get('find')
  async getOne(@Query('id') id: number): Promise<Users> {
    return await this.userService.getUser(id);
  }

  @ApiCreatedResponse({ description: 'User created succesfully' })
  @HttpCode(200)
  @Post('new')
  async createUser(@Body() createUser: CreateUserDto): Promise<Users> {
    return await this.userService.createUser(createUser);
  }

  @ApiCreatedResponse({ description: 'User updated successfully' })
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  @Put('update')
  async updateUser(
    @Body() createUserDto,
    @Query('id') id: number,
  ): Promise<Users> {
    return await this.userService.updateUser(id, createUserDto);
  }

  @ApiCreatedResponse({ description: 'User eliminated successfully' })
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  @Delete('delete')
  async deleteUser(@Query('id') id: number): Promise<TransactionResposeDto> {
    return await this.userService.deleteUser(id);
  }
}
