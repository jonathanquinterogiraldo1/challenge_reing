import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from '@nestjs/class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  id: number;
  name: string;
  email: string;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export class TokenDto {
  token: string;
}

export class LoginUserRequestDto {
  @ApiProperty({
    description: 'Email registered in data base',
    example: 'jhon_doe@example.com',
  })
  @IsNotEmpty()
  @IsEmail(
    {},
    {
      message: 'Email can not be empty',
    },
  )
  email: string;

  @ApiProperty({
    description: 'User password',
    example: '123ABC',
  })
  @IsString()
  @IsNotEmpty({
    message: 'Password can noto be empt',
  })
  password: string;
}

export class UpdateUserDto {
  createdAt?: Date;
  updatedAt?: Date;

  @ApiProperty({
    description: 'Full user name',
    example: 'Jhon Doe',
  })
  @IsString()
  name?: string;

  @ApiProperty({
    description: 'Email registered in data base',
    example: 'jhon_doe@example.com',
  })
  @IsEmail({})
  email?: string;

  @ApiProperty({
    description: 'User password',
    example: '123456ABC',
  })
  @IsString()
  @MinLength(4, {
    message: 'Password must be greater than 4 characters',
  })
  @MaxLength(20, {
    message: 'Password must not be less than 20 characters',
  })
  password?: string;
}

export class CreateUserDto {
  id: number;
  createdAt?: Date;

  @IsOptional()
  updatedAt?: Date;

  @ApiProperty({
    description: 'Full user name',
    example: 'Jhon Doe',
  })
  @IsString()
  @IsNotEmpty({
    message: 'Name can not be empty',
  })
  name: string;

  @ApiProperty({
    description: 'Email registered in data base',
    example: 'jhon_doe@example.com',
  })
  @IsEmail(
    {},
    {
      message: 'Email can not be empty',
    },
  )
  email: string;

  @ApiProperty({
    description: 'User password',
    example: '123456ABC',
  })
  @IsString()
  @IsNotEmpty({
    message: 'Password can not be empty',
  })
  @MinLength(4, {
    message: 'Password must be greater than 4 characters',
  })
  @MaxLength(20, {
    message: 'Password must not be less than 20 characters',
  })
  password: string;
}
