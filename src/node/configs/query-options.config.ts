export class QueryOptions {
  offset?: number;
  limit?: string;
  fields?: string;
  text?: string;
  page?: string;
  month?: string;
}
