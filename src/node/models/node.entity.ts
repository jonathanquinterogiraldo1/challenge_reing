import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NodeDocument = Node & Document;

@Schema()
export class Node {
  @Prop()
  created_at: Date;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: number;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop()
  _tags: [];

  @Prop()
  objectID: number;
  _highlightResult: {
    title: {
      value: string;
      matchLevel: string;
      matchedWords: [];
    };
    url: {
      value: string;
      matchLevel: string;
      fullyHighlighted: boolean;
      matchedWords: [];
    };
    author: {
      value: string;
      matchLevel: string;
      matchedWords: [];
    };
  };
}

export const NodeSchema = SchemaFactory.createForClass(Node);
