import {
  Controller,
  Post,
  Get,
  HttpCode,
  UseGuards,
  Query,
  Delete,
} from '@nestjs/common';
import { NodeService } from '../services/node.service';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { TransactionResposeDto, GetNewDataResponseDto } from '../dto/node.dto';
import { JwtAuthGuard } from '../../auth/guards/jwt.guard';

@UseGuards(JwtAuthGuard)
@Controller('api/node')
export class NodeController {
  constructor(private nodeService: NodeService) {}

  @ApiCreatedResponse({ description: 'Data fetched and saved successfully!' })
  @HttpCode(200)
  @Post('save_news')
  async saveNews(): Promise<TransactionResposeDto> {
    return await this.nodeService.getAndSaveNodeNews();
  }

  @ApiCreatedResponse({ description: 'Get news from database successfully!' })
  @HttpCode(200)
  @Get('get_news')
  async getNews(@Query() queryParams: any): Promise<GetNewDataResponseDto> {
    return await this.nodeService.getNodeNewsFromDataBase(queryParams);
  }

  @ApiCreatedResponse({ description: 'Element deleted successfully!' })
  @HttpCode(200)
  @Delete('delete')
  async deleteNews(@Query('id') id: number): Promise<TransactionResposeDto> {
    return this.nodeService.deleteNodeNew(id);
  }
}
