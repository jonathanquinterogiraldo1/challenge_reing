import { Test, TestingModule } from '@nestjs/testing';
import { NodeService } from '../services/node.service';
import { getModelToken } from '@nestjs/mongoose';
import { Node } from '../models/node.entity';
import * as nock from 'nock';
const mockingoose = require('mockingoose');
require('dotenv').config();

describe('NodeService', () => {
  let service: NodeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NodeService,
        {
          provide: getModelToken(Node.name),
          useClass: Node,
        },
      ],
    }).compile();

    service = module.get<NodeService>(NodeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return when try to fetchdata from api', async () => {
    try {
      nock('https://hn.algolia.com')
        .post('/api/v1/search_by_date?query=nodejs')
        .reply(400, { error: 'Bad Request' });
      await service.getAndSaveNodeNews();
    } catch (error) {
      expect(error.name).toBe('HttpException');
    }
  });

  it('should format month and return number of month', async () => {
    try {
      nock('https://hn.algolia.com')
        .post('/api/v1/search_by_date?query=nodejs')
        .reply(400, { error: 'Bad Request' });
      await service.deleteNodeNew(1);
    } catch (error) {
      expect(error.name).toBe('HttpException');
    }
  });

  it('should format month and return number of month', async () => {
    try {
      nock('https://hn.algolia.com')
        .post('/api/v1/search_by_date?query=nodejs')
        .reply(400, { error: 'Bad Request' });
      await service.getNodeNewsFromDataBase(null);
    } catch (error) {
      expect(error.name).toBe('HttpException');
    }
  });

  it('should format month and return number of month', () => {
    const response = service.formatMonth('september');
    expect(response).toBe('09');
  });

  it('should format query parameters to find', () => {
    const response = service.formatDateToFind('09');
    expect(response.dateIni.toString()).toBe(
      'Thu Sep 01 2022 00:00:00 GMT-0500 (Colombia Standard Time)',
    );
  });

  it('should format query parameters fields to find', () => {
    const response = service.buildQuerySearch({
      author: 'Name',
      _tags: 'tag1',
    });
    expect(response.$and.length).toBe(2);
  });

  it('should format query parameters month to find', () => {
    const response = service.buildQuerySearch({ month: 'january' });
    expect(response.$and.length).toBe(1);
  });
});
