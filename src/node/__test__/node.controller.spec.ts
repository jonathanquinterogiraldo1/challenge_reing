import { Test, TestingModule } from '@nestjs/testing';
import { NodeController } from '../controllers/node.controller';
import { NodeService } from '../services/node.service';

describe('NodeController', () => {
  let controller: NodeController;

  const nodeNew = {
    id: '6330dd8ee0d66329606782ac',
    created_at: '2022-09-24T00:51:03.000Z',
    title: 'Node.js v18.9.1',
    url: 'https://nodejs.org/en/blog/release/v18.9.1/',
    author: 'bricss',
    points: 2,
    story_text: null,
    comment_text: null,
    num_comments: 0,
    story_id: null,
    story_title: null,
    story_url: null,
    parent_id: null,
    created_at_i: 1663980663,
    _tags: ['story', 'author_bricss', 'story_32958781'],
    objectID: 32958781,
    __v: 0,
  };

  const mockNodeService = {
    getAndSaveNodeNews: jest.fn(
      () => '20 elements fetched and saved on data base successfully!',
    ),
    getNodeNewsFromDataBase: jest.fn().mockResolvedValue({
      data: [nodeNew, nodeNew],
      page: 1,
      limit_x_page: 5,
      total_found: 2,
      last_page: 1,
    }),
    deleteNodeNew: jest.fn(() => 'Element deleted succesfully!'),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NodeController],
      providers: [NodeService],
    })
      .overrideProvider(NodeService)
      .useValue(mockNodeService)
      .compile();

    controller = module.get<NodeController>(NodeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should retornar message after saved node news', async () => {
    const response = await controller.saveNews();
    expect(response).toBe(
      '20 elements fetched and saved on data base successfully!',
    );
  });

  it('should retornar list of node news inserted', async () => {
    const response = await controller.getNews({
      author: 'test',
      month: 'september',
    });
    expect(response.page).toBe(1);
  });

  it('should retornar list of node news inserted', async () => {
    const response = await controller.deleteNews(1);
    expect(response).toBe('Element deleted succesfully!');
  });
});
