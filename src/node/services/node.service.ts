import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import axios from 'axios';
import { Model } from 'mongoose';
import {
  RangeDatetoFindDto,
  GetNewDataResponseDto,
  TransactionResposeDto,
  QueryRequestDto,
} from '../dto/node.dto';
import { Node, NodeDocument } from '../models/node.entity';
import { QueryOptions } from '../configs/query-options.config';
import { Cron, CronExpression } from '@nestjs/schedule';
require('dotenv').config();

@Injectable()
export class NodeService {
  constructor(
    @InjectModel(Node.name) private readonly nodeNewModel: Model<NodeDocument>,
  ) {}

  private readonly logger = new Logger(NodeService.name);

  @Cron(CronExpression.EVERY_HOUR)
  async fetchAndSaveDataAutomatically() {
    try {
      const data = await Promise.all([
        this.getAndSaveNodeNews(),
        this.nodeNewModel.count(null),
      ]);
      this.logger.debug(
        `Scheduled task completed succesfully and there are ${data[1]} elements on data base!`,
      );
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async getAndSaveNodeNews(): Promise<TransactionResposeDto> {
    try {
      this.logger.debug(`Fetingh data from ${process.env.URL_ENDPOINT} ...`);
      const {
        data: { hits },
      } = await axios({
        method: 'get',
        timeout: 5000,
        url: process.env.URL_ENDPOINT,
      });

      hits.forEach((element) => this.saveNodeNews(element));
      this.logger.debug(
        `${hits.length} elements fetched and saved on data base successfully!`,
      );
      return {
        message: 'Data saved successfully!',
      };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async saveNodeNews(nodeNew: Node): Promise<void> {
    try {
      await new this.nodeNewModel(nodeNew).save();
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async getNodeNewsFromDataBase(
    queryOptions: QueryOptions,
  ): Promise<GetNewDataResponseDto> {
    try {
      const optionsFind = this.buildQuerySearch(queryOptions);
      const page: number = parseInt(queryOptions.page) || 1;
      const limit: number = parseInt(queryOptions.limit)
        ? parseInt(queryOptions.limit)
        : 5;
      const total_found = await this.nodeNewModel.count(optionsFind);

      const data = await this.nodeNewModel
        .find(optionsFind)
        .skip((page - 1) * limit)
        .limit(limit)
        .sort('created_at');
      this.logger.debug(`${total_found} elements found!`);
      return {
        data,
        total_found,
        limit_x_page: limit,
        page,
        last_page: Math.ceil(total_found / limit),
      };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async deleteNodeNew(id: number): Promise<TransactionResposeDto> {
    try {
      const { deletedCount } = await this.nodeNewModel.deleteOne({ _id: id });
      if (deletedCount) {
        this.logger.debug(`Element deleted with id ${id}`);
        return { message: 'Element deleted succesfully!' };
      } else
        throw new HttpException(
          'There is not element to delete!',
          HttpStatus.BAD_REQUEST,
        );
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  buildQuerySearch(query: QueryRequestDto) {
    const options = [];
    const configOptions = ['limit', 'offset', 'page'];
    for (const property in query) {
      if (!configOptions.includes(property)) {
        const obj = {};
        if (property !== 'month')
          obj[property] = new RegExp(query[property], 'i');
        else {
          const monthFormated = this.formatMonth(query[property]);
          const { dateIni, dateEnd } = this.formatDateToFind(monthFormated);
          obj['created_at'] = { $gte: dateIni, $lt: dateEnd };
        }
        options.push(obj);
      }
    }
    return options.length > 0 ? { $and: options } : null;
  }

  formatMonth(monthString: string): string {
    const monthNumber =
      [
        'january',
        'february',
        'march',
        'april',
        'may',
        'june',
        'july',
        'august',
        'september',
        'october',
        'november',
        'december',
      ].indexOf(monthString.toLowerCase()) + 1;
    return monthNumber.toString().padStart(2, '0');
  }

  formatDateToFind(monthNumber): RangeDatetoFindDto {
    const year = new Date().getFullYear();
    const nextMonthFirstDay = new Date(year, monthNumber, 1);
    const dateIni = new Date(year, monthNumber - 1, 1);
    const dateEnd = DaysAdd(nextMonthFirstDay, -1);

    function DaysAdd(date, days) {
      date.setDate(date.getDate() + days);
      return date;
    }
    return {
      dateIni,
      dateEnd,
    };
  }
}
