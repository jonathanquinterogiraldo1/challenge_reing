export class TransactionResposeDto {
  message: string;
}

export class QueryRequestDto {
  author?: string;
  _tags?: string;
  title?: string;
  month?: string;
}

export class NodeNewDto {
  created_at: Date;
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: [];
  objectID: number;
  _highlightResult: {
    title: {
      value: string;
      matchLevel: string;
      matchedWords: {};
    };

    url: {
      value: string;
      matchLevel: string;
      fullyHighlighted: boolean;
      matchedWords: {};
    };

    author: {
      value: string;
      matchLevel: string;
      matchedWords: {};
    };
  };
}

export class GetNewDataResponseDto {
  data: NodeNewDto[];
  page: number;
  limit_x_page: number;
  total_found: number;
  last_page: number;
}

export class RangeDatetoFindDto {
  dateIni: Date;
  dateEnd: Date;
}
