import { Schema } from 'mongoose';

export const NodeSchema = new Schema({
  created_at: Date,
  title: String,
  url: String,
  author: String,
  points: Number,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: [],
  objectID: Number,
  _highlightResult: {
    title: {
      value: String,
      matchLevel: String,
      matchedWords: [],
    },
    url: {
      value: String,
      matchLevel: String,
      fullyHighlighted: Boolean,
      matchedWords: [],
    },
    author: {
      value: String,
      matchLevel: String,
      matchedWords: [],
    },
  },
});
