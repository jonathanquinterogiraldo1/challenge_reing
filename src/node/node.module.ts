import { Module } from '@nestjs/common';
import { NodeController } from './controllers/node.controller';
import { NodeService } from './services/node.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Node, NodeSchema } from '../node/models/node.entity';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forFeature([{ name: Node.name, schema: NodeSchema }]),
  ],
  providers: [NodeService],
  controllers: [NodeController],
})
export class NodeModule {}
